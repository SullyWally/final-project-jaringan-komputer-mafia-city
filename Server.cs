﻿/*
 * Created by SharpDevelop.
 * User: sully
 * Date: 14/05/2019
 * Time: 12:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace Testing_Server
{
	public static class gvar{
		public static int count = 0;
		public static string phase = "day";
		public static string [] roles = {"Mayor","Detective","Policeman","Doctor","Civilian","Civilian","Boss","Hitman","Spy"};
		public static int [] rolesTaken = {10,10,10,10,10,10,10,10,10};
        public static int takenRole = 0;
        public static string [] playerList={"","","","","","","","",""};
        public static int [] voting={0,0,0,0,0,0,0,0,0};
        public static string [] status = {"","","","","","","","",""};
        public static int playerAlive = 0;
        public static bool [] playerDead = {false,false,false,false,false,false,false,false,false};
        public static bool [] mafia = {false,false,false,false,false,false,false,false,false};
        public static int mafiacount = 0;
        public static int towncount = 0;
        public static int playerlimit = 4;
	}
	class Program
{
    static readonly object _lock = new object();
    static readonly Dictionary<int, TcpClient> list_clients = new Dictionary<int, TcpClient>();

    static void Main(string[] args)
    {
        TcpListener ServerSocket = new TcpListener(IPAddress.Any, 5000);
        ServerSocket.Start();
        
        while (gvar.count != gvar.playerlimit - 1)
        {
            TcpClient client = ServerSocket.AcceptTcpClient();
            lock (_lock) list_clients.Add(gvar.count, client);
            

            Thread t = new Thread(handle_clients);
            t.Start(gvar.count);
            Console.WriteLine(gvar.count + 1 + " Player is on the server.");
        }
        while (gvar.count != gvar.playerlimit){
        		
        }
        broadcast("Game Started!");
        for (int i = 0;i<gvar.playerlimit;i++){
        	Console.WriteLine(i+1 + ". " + gvar.playerList[i] + " : " + gvar.roles[gvar.rolesTaken[i]]);
        }
        System.Threading.Timer dayNight = new Timer(countdown,10,1,30000);
        
        
        
        
    }

    public static void handle_clients(object o)
    {
        int id = (int)o;
        bool actionUsed = false;
        bool voted = false;
        
        TcpClient client;

        lock (_lock) client = list_clients[id];
		NetworkStream stream = client.GetStream();
        byte[] buffer = new byte[1024];
        
        int byte_count = stream.Read(buffer, 0, buffer.Length);
        string data = Encoding.ASCII.GetString(buffer, 0, byte_count);
        string username = data;
        gvar.playerList[gvar.count] = username;
        gvar.count++;
       	
        string PlayerRole = gvar.roles[RoleAssignment()];
        int playerNum = gvar.takenRole - 1;
       	gvar.playerAlive++;
        do{
        	
       	} while (gvar.count !=gvar.playerlimit);
       	
        buffer = Encoding.ASCII.GetBytes("Your Roles is " + PlayerRole);
        stream.Write(buffer, 0, buffer.Length);
        buffer = Encoding.ASCII.GetBytes("\n");
        stream.Write(buffer, 0, buffer.Length);
        
        if (PlayerRole == "Boss" || PlayerRole == "Hitman" || PlayerRole == "Spy"){
        	buffer = Encoding.ASCII.GetBytes("Your mafia are : ");
        	stream.Write(buffer, 0, buffer.Length);
        	buffer = Encoding.ASCII.GetBytes("\n");
        	stream.Write(buffer, 0, buffer.Length);
        	for (int i = 0 ; i< gvar.playerlimit; i++){
        		if (gvar.mafia[i]){
        			buffer = Encoding.ASCII.GetBytes(gvar.playerList[i]);
        			stream.Write(buffer, 0, buffer.Length);
        			buffer = Encoding.ASCII.GetBytes("\n");
        			stream.Write(buffer, 0, buffer.Length);
        		}
        	}
        }
       
         for (int i = 0; i < gvar.playerlimit; i++){
	            	buffer = Encoding.ASCII.GetBytes( i + 1 +". " + gvar.playerList[i]);
       				stream.Write(buffer, 0, buffer.Length);
       				buffer = Encoding.ASCII.GetBytes("\n");
       				stream.Write(buffer, 0, buffer.Length);
	    	 }
   
        
        while (gvar.count == gvar.playerlimit && (gvar.mafiacount != 0 && gvar.towncount != 0))
        {
        	stream = client.GetStream();
	        buffer = new byte[1024];
      		byte_count = stream.Read(buffer, 0, buffer.Length);
        	data = Encoding.ASCII.GetString(buffer, 0, byte_count);
        	if (gvar.phase == "voting"){ // day phase
        		actionUsed = false;
	            
        		if (PlayerRole == "Mayor" && (data == "reveal" || data == "Reveal")){
	            	broadcast(username + " has reveal themselves as the Mayor.");
	            	PlayerRole = "MayorR";
	            }
        		if (gvar.playerDead[playerNum] == false){ // only broadcast if alive
        			broadcast(username + ": " + data);
        		}
	            Console.WriteLine(username + ": " + data);
        	}
        	
        	if (gvar.phase == "night"){ // voting phase
        		
	            int num = Int32.Parse(data);
	            if (voted == false){
	            	voted = true;
	            	if (PlayerRole == "MayorR")
	            		gvar.voting[num-1] += 2;
	            	else
	            		gvar.voting[num-1] += 1;
	            }
        	}
        	
        	
        	else if (gvar.phase == "day"){ // night phase
        		int num = Int32.Parse(data);
       
        		if (PlayerRole == "Doctor"){
	            	
	            	if(!actionUsed){
		            	gvar.status[num-1] = "healed";
		            	actionUsed = true;
	            	}
        		}
        		if (PlayerRole == "hitman"){
        			
	            	if(!actionUsed){
		            	if(gvar.status[num-1] != "healed")
		            		gvar.status[num-1] = "kill";
		            	actionUsed=true;
	            	}
        		}
        		if (PlayerRole == "Policeman"){
        			
	            	if(!actionUsed){
		            	if(gvar.status[num-1] != "healed")
		            		gvar.status[num-1] = "kill";
		            	actionUsed=true;
	            	}
        		}
        		if(PlayerRole == "Boss"){
        			
	            	if(!actionUsed){
	            		if(gvar.status[num-1] != "healed")
	            			gvar.status[num-1] = "kill";
	            		actionUsed = true;
	            	}
        		}
        		if(PlayerRole == "Detective"){
	            	
	            	if (actionUsed == false){
	            		actionUsed = true;
	            		if(gvar.roles[gvar.rolesTaken[num-1]] == "Boss" || gvar.roles[gvar.rolesTaken[num-1]] == "Mayor" || gvar.roles[gvar.rolesTaken[num-1]] == "Doctor"){
	            			 buffer = Encoding.ASCII.GetBytes("Your Target is either a Boss, Mayor, or Doctor");
       						 stream.Write(buffer, 0, buffer.Length);
       						 buffer = Encoding.ASCII.GetBytes("\n");
       						 stream.Write(buffer, 0, buffer.Length);
	            		}
	            		if(gvar.roles[gvar.rolesTaken[num-1]] == "Policeman" || gvar.roles[gvar.rolesTaken[num-1]] == "Hitman"){
	            			 buffer = Encoding.ASCII.GetBytes("Your Target is either a Policeman or Hitman");
       						 stream.Write(buffer, 0, buffer.Length);
       						 buffer = Encoding.ASCII.GetBytes("\n");
       						 stream.Write(buffer, 0, buffer.Length);
	            		}
	            		if(gvar.roles[gvar.rolesTaken[num-1]] == "Spy" || gvar.roles[gvar.rolesTaken[num-1]] == "Civilian"){
	            			 buffer = Encoding.ASCII.GetBytes("Your Target is either a Spy or Civilian");
       						 stream.Write(buffer, 0, buffer.Length);
       						 buffer = Encoding.ASCII.GetBytes("\n");
       						 stream.Write(buffer, 0, buffer.Length);
	            		}
	            	}
	            	else {
	            			buffer = Encoding.ASCII.GetBytes("You already investigate your target tonight.");
       						stream.Write(buffer, 0, buffer.Length);
	            	}
	            	
        		}
        		if(PlayerRole == "Spy"){
        			byte_count = stream.Read(buffer, 0, buffer.Length);
	            	data = Encoding.ASCII.GetString(buffer, 0, byte_count);
	            	gvar.roles[gvar.rolesTaken[playerNum]] = gvar.roles[gvar.rolesTaken[num-1]];
        		}
        	}
        }
        
        if(gvar.towncount == 0){
        	buffer = Encoding.ASCII.GetBytes("Mafia has win the Game!");
       		stream.Write(buffer, 0, buffer.Length);
        }
        else if (gvar.mafiacount == 0){
        	buffer = Encoding.ASCII.GetBytes("Town has win the Game!");
       		stream.Write(buffer, 0, buffer.Length);
        }
		
        lock (_lock) list_clients.Remove(id);
        client.Client.Shutdown(SocketShutdown.Both);
        client.Close();
    }
       

    public static void broadcast(string data)
    {
        byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

        lock (_lock)
        {
            foreach (TcpClient c in list_clients.Values)
            {
                NetworkStream stream = c.GetStream();

                stream.Write(buffer, 0, buffer.Length);
            }
        }
    }
    static void countdown (object args){
        	if(gvar.phase == "day"){
        		broadcast("Day Time");
        		for(int i=0; i<= 8; i++){ // death announcement
    				if (gvar.status[i] == "kill"){
    					broadcast(gvar.playerList[i] + " has been killed. Their Role is " + gvar.roles[gvar.rolesTaken[i]]);
    					gvar.playerDead[i] = true;
    					gvar.status[i] = "";
    					if (gvar.roles[gvar.rolesTaken[i]] == "Boss" || gvar.roles[gvar.rolesTaken[i]] == "Spy" || gvar.roles[gvar.rolesTaken[i]] == "Hitman"){
    						gvar.mafiacount--;
    					}
    					else{
    						gvar.towncount--;
    					}
    				}
    			}
        		gvar.phase = "voting";
        	}
        	else if (gvar.phase == "voting"){
        		broadcast("Voting Time");
        		broadcast("Vote one person to be executed. (Choose their number)");
        		gvar.phase = "night";
        	}
        	else if (gvar.phase == "night"){
    		for (int i=0; i<= 8; i++){ // voting result
    			if (gvar.voting[i] >= (gvar.playerAlive / 2 + 1)){
    				broadcast(gvar.playerList[i] + " has been voted guilty. Their Role is " + gvar.roles[gvar.rolesTaken[i]]);
    				gvar.playerDead[i] = true;
    				if (gvar.roles[gvar.rolesTaken[i]] == "Boss" || gvar.roles[gvar.rolesTaken[i]] == "Spy" || gvar.roles[gvar.rolesTaken[i]] == "Hitman"){
    					gvar.mafiacount--;
    				}
    				else{
    					gvar.towncount--;
    				}
    				break;
    			}
    		}
    		
    		for (int i = 0; i<= 8; i++){ // reset vote count
    			gvar.voting[i] = 0;
    		}
    			broadcast("Night time");
    			broadcast("Do your action for the night. (Choose their number)");
        		gvar.phase = "day";
        	}
        }
    static int RoleAssignment(){
    	
    	// roles enter to gvar.Roles[rolesTaken[int]]
    	Random rnd = new Random();
    	int roleAssign = 0;
    	bool getRoles = false;
    	while(!getRoles){
	        roleAssign = rnd.Next(9);
	        getRoles = true;
	        for (int i=0; i < 9; i++){
	        	if (roleAssign == gvar.rolesTaken[i]){
	        		getRoles = false;
	        		break;
	        	}
	        }
    	}
    	gvar.rolesTaken[gvar.takenRole] = roleAssign;
    	if (gvar.roles[roleAssign] == "Boss" || gvar.roles[roleAssign] == "Hitman" || gvar.roles[roleAssign] == "Spy"){
    		gvar.mafia[gvar.takenRole] = true;
    		gvar.mafiacount++;
    	}
    	else{
    		gvar.towncount++;
    	}
    	gvar.takenRole++;
    	return roleAssign;
    	
    }
}
}